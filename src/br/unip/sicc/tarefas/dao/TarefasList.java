package br.unip.sicc.tarefas.dao;

import br.unip.sicc.tarefas.model.Categoria;
import br.unip.sicc.tarefas.model.Situacao;
import br.unip.sicc.tarefas.model.Tarefa;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TarefasList implements TarefasDao {
    private static int NEXT_ID;

    private List<Tarefa> tarefas;
    private final List<Tarefa> tarefasFiltradas;

    public TarefasList() {
        this.tarefasFiltradas = new ArrayList<>();
        this.tarefas = new ArrayList<>();
        long time = new Date().getTime();
        long umaHora = 1000*60*60;
        long umDia = umaHora*24;
        long umaSemana;
        umaSemana = umDia*7;
        Tarefa tarefa1 = new Tarefa(1,new Date(time-umaHora),"Aula ALPOO", 1,
                Categoria.TRABALHO,Situacao.EM_ANDAMENTO);
        tarefas = new ArrayList<>();
        
        tarefas.add(tarefa1);
        tarefas.add(new Tarefa(2,new Date(time-umaSemana),"Comprar Cerveja", 1,
                Categoria.PESSOAL,Situacao.PENDENTE));
        tarefas.add(new Tarefa(3,new Date(time-umDia),"Implantar Consulta de Cliente", 3,
                Categoria.TRABALHO,Situacao.PENDENTE));
        tarefas.add(new Tarefa(4,new Date(time-umDia*2),"Ler livro Java Core", 5,
                Categoria.ESTUDO,Situacao.PENDENTE));
        tarefas.add(new Tarefa(5,new Date(time-umaSemana),"Ir na churrascaria", 1,
                Categoria.PESSOAL,Situacao.PENDENTE));
        NEXT_ID = 6;
    }

    
    
    @Override
    public void incluir(Tarefa tarefa) throws DaoException {
        if(tarefa.eNova()){
            tarefa.setId(NEXT_ID++);
            tarefas.add(tarefa);
        }
    }

    @Override
    public void atualizar(Tarefa tarefa) throws DaoException {
        if (!tarefas.contains(tarefa)) {
            throw new DaoException("Tarefa não existe");
        }else{
            int indice = tarefas.indexOf(tarefa);
            tarefas.set(indice, tarefa);
        }
    }

    @Override
    public void excluir(Tarefa tarefa) throws DaoException {
        if (!tarefas.contains(tarefa)) {
            throw new DaoException("Tarefa não existe");
        } else {
            tarefas.remove(tarefa);
        }
    }

    @Override
    public List<Tarefa> getTarefas() throws DaoException {
        return tarefas;
    }

    @Override
    public List<Tarefa> getTarefas(Tarefa filtro) throws DaoException {
        tarefasFiltradas.clear();
        if (!filtro.getId().equals(0)) {
            return getTarefasById(filtro);
        }
        for (Tarefa tarefa : tarefas) {
            String descricaoFiltro = filtro.getDescricao().toUpperCase();
            String descricaoTarefa = tarefa.getDescricao().toUpperCase();
            if (descricaoTarefa.contains(descricaoFiltro)) {
                tarefasFiltradas.add(tarefa);
            }
        }
        return tarefasFiltradas;
    }

    private List<Tarefa> getTarefasById(Tarefa filtro) {
        for (Tarefa tarefa : tarefas) {
            if (tarefa.getId().equals(filtro.getId())) {
                tarefasFiltradas.add(tarefa);
                return tarefasFiltradas;
            }
        }
        return tarefasFiltradas;
    }

}
