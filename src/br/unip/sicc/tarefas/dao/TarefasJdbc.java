package br.unip.sicc.tarefas.dao;

import br.unip.sicc.tarefas.model.Categoria;
import br.unip.sicc.tarefas.model.Situacao;
import br.unip.sicc.tarefas.model.Tarefa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class TarefasJdbc implements TarefasDao {

    private static final String SQL_CREATE_TABLE
            = "CREATE TABLE IF NOT EXISTS TB_TAREFAS( "
            + "ID int(3) NOT NULL AUTO_INCREMENT PRIMARY KEY, "
            + "DATA DATE NOT NULL, "
            + "DESCRICAO VARCHAR(255) NOT NULL, "
            + "PRIORIDADE int(3), "
            + "CATEGORIA varchar(255), "
            + "SITUACAO varchar(255))";

    public void criarTabela() throws DaoException {
        Connection conexao = null;
        Statement comando = null;
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.createStatement();
        } catch (SQLException ex) {
            throw new DaoException("Problema ao criar tabela", ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando);
        }
    }

    private static final String SQL_INSERT
            = "INSERT INTO TB_TAREFAS (DATA, DESCRICAO, PRIORIDADE, CATEGORIA, SITUACAO) "
            + "VALUES (?, ?, ?, ?, ?)";

    @Override
    public void incluir(Tarefa tarefa) throws DaoException {
        Connection conexao = null;
        PreparedStatement comando = null;
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.prepareStatement(SQL_INSERT);
            java.sql.Date data = new java.sql.Date(tarefa.getData().getTime());
            comando.setDate(1, data);
            comando.setString(2, tarefa.getDescricao());
            comando.setInt(3, tarefa.getPrioridade());
            comando.setString(4, tarefa.getCategoria().name());
            comando.setString(5, tarefa.getSituacao().name());
            int qtdeRegistros = comando.executeUpdate();
            System.out.println(qtdeRegistros + " inclusão");
        } catch (SQLException ex) {
            throw new DaoException("Problema incluir tarefa: " + tarefa, ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando);
        }
    }
    private static final String SQL_UPDATE
            = "UPDATE TB_TAREFAS SET DATA=?, DESCRICAO=?, PRIORIDADE=?, CATEGORIA=?, SITUACAO=? WHERE ID=?";

    @Override
    public void atualizar(Tarefa tarefa) throws DaoException {
        Connection conexao = null;
        PreparedStatement comando = null;
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.prepareStatement(SQL_UPDATE);
            java.sql.Date data = new java.sql.Date(tarefa.getData().getTime());
            comando.setDate(1, data);
            comando.setString(2, tarefa.getDescricao());
            comando.setInt(3, tarefa.getPrioridade());
            comando.setString(4, tarefa.getCategoria().name());
            comando.setString(5, tarefa.getSituacao().name());
            comando.setInt(6, tarefa.getId());
            int qtdeRegistros = comando.executeUpdate();
            System.out.println(qtdeRegistros + " atualização");
        } catch (SQLException ex) {
            throw new DaoException("Problema atualizar tarefa: " + tarefa, ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando);
        }
    }

    private static final String SQL_DELETE
            = "DELETE FROM TB_TAREFAS WHERE id = ?";

    @Override
    public void excluir(Tarefa tarefa) throws DaoException {
        Connection conexao = null;
        PreparedStatement comando = null;
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.prepareStatement(SQL_DELETE);
            comando.setInt(1, tarefa.getId());
            int qtdeRegistros = comando.executeUpdate();
            System.out.println(qtdeRegistros + " exclusão");
        } catch (SQLException ex) {
            throw new DaoException("Problema excluir tarefa: " + tarefa, ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando);
        }
    }

    private static final String SQL_SELECT_ALL
            = "SELECT ID, DATA, DESCRICAO, PRIORIDADE, CATEGORIA, SITUACAO FROM TB_TAREFAS;";

    @Override
    public List<Tarefa> getTarefas() throws DaoException {
        Connection conexao = null;
        PreparedStatement comando = null;
        ResultSet resultados = null;
        List<Tarefa> tarefas = new ArrayList<>();
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.prepareStatement(SQL_SELECT_ALL);
            resultados = comando.executeQuery();
            while (resultados.next()) {
                int id = resultados.getInt("ID");
                java.sql.Date data = resultados.getDate("DATA");
                String descricao = resultados.getString("DESCRICAO");
                int prioridade = resultados.getInt("PRIORIDADE");
                String categoriaStr = resultados.getString("CATEGORIA");
                String situacaoStr = resultados.getString("SITUACAO");
                Tarefa tarefa = new Tarefa(id, data, descricao, prioridade,
                        Categoria.valueOf(categoriaStr), Situacao.valueOf(situacaoStr));
                tarefas.add(tarefa);
            }
            System.out.println(tarefas.size() + " recuperados");
        } catch (SQLException ex) {
            throw new DaoException("Problema recuperar todos tarefas: ", ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando, resultados);
        }
        return tarefas;
    }

    private static final String SQL_SELECT_BY_DESCRICAO
            = "SELECT ID, DATA, DESCRICAO, PRIORIDADE, CATEGORIA, SITUACAO FROM TB_TAREFAS "
            + "WHERE DESCRICAO like ?";

    @Override
    public List<Tarefa> getTarefas(Tarefa filtro) throws DaoException {
        Connection conexao = null;
        PreparedStatement comando = null;
        ResultSet resultados = null;
        List<Tarefa> tarefas = new ArrayList<>();
        try {
            conexao = GerenciadorDeConexao.getConexao();
            comando = conexao.prepareStatement(SQL_SELECT_BY_DESCRICAO);
            comando.setString(1, "%" + filtro.getDescricao()+ "%");
            resultados = comando.executeQuery();
            while (resultados.next()) {
                int id = resultados.getInt("ID");
                java.sql.Date data = resultados.getDate("DATA");
                String descricao = resultados.getString("DESCRICAO");
                int prioridade = resultados.getInt("PRIORIDADE");
                String categoriaStr = resultados.getString("CATEGORIA");
                String situacaoStr = resultados.getString("SITUACAO");
                Tarefa tarefa = new Tarefa(id, data, descricao, prioridade,
                        Categoria.valueOf(categoriaStr), Situacao.valueOf(situacaoStr));
                tarefas.add(tarefa);
            }
        } catch (SQLException ex) {
            throw new DaoException("Problema recuperar todos tarefas: ", ex);
        } finally {
            GerenciadorDeConexao.fechar(conexao, comando, resultados);
        }
        return tarefas;
    }

}
