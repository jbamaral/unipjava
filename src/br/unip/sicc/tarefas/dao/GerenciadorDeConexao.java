package br.unip.sicc.tarefas.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GerenciadorDeConexao {

    private static final String IP = "127.0.0.1";
    private static final String PORTA = "3306";
    private static final String BD = "tarefas";
    private static final String USER = "unip";
    private static final String PASS = "unip";
    private static final String URL_CONEXAO = "jdbc:mysql://" + IP + ":" + PORTA + "/" + BD;

    public static Connection getConexao() throws DaoException {
        Connection conexao = null;
        try {
            conexao = DriverManager.getConnection(URL_CONEXAO, USER, PASS);
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DaoException("Não foi possível conectar", ex);
        }
        return conexao;
    }

    public static void fechar(Connection conexao) throws DaoException {
        try {
            if (conexao != null && !conexao.isClosed()) {
                conexao.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DaoException("Não foi possível fechar a conexão", ex);
        }
    }
    public static void fechar(Connection conexao, Statement comando) throws DaoException {
        try {
            if (comando != null && !comando.isClosed()) {
                comando.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DaoException("Não foi possível fechar o comando", ex);
        }finally{
            GerenciadorDeConexao.fechar(conexao);
        }
    }
    public static void fechar(Connection conexao, Statement comando, ResultSet resultados) throws DaoException {
        try {
            if (resultados != null && !resultados.isClosed()) {
                resultados.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            throw new DaoException("Não foi possível fechar os resultados", ex);
        }finally{
            GerenciadorDeConexao.fechar(conexao, comando);
        }
    }
}

