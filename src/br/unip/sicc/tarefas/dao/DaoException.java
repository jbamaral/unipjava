package br.unip.sicc.tarefas.dao;

public class DaoException extends Exception {
    private static final long serialVersionUID = 831553540352535179L;

    public DaoException() {
    }

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }
    
    
    
}
