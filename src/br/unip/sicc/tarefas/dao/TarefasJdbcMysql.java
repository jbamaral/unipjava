package br.unip.sicc.tarefas.dao;


import br.unip.sicc.tarefas.model.Categoria;
import br.unip.sicc.tarefas.model.Situacao;
import br.unip.sicc.tarefas.model.Tarefa;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TarefasJdbcMysql implements TarefasDao {
    Connection db;
    List<Tarefa> tarefas;
    boolean hasChanged;
    String lastFilter;
    
    private final String datePattern = "YYYYMMdd";
    private final SimpleDateFormat dateFormatter = 
                                              new SimpleDateFormat(datePattern);    
    public TarefasJdbcMysql() {
	db = new ConnectionFactory("com.mysql.jdbc.Driver",
		      "jdbc:mysql://localhost:3306/tarefas", 
                      "unip", "unip").getConnection();
        tarefas = null;
        hasChanged = false;
        lastFilter = "";
    }
    
    private void execUpdate(String sql) {
        System.out.println(sql);
        try {
            Statement stmt = db.createStatement();
            stmt.executeUpdate(sql);
            hasChanged = true;
        } catch (Exception e) {
            System.out.println(e);               
        }
    }
    
    private String toSqlDate(Date d) {
        return dateFormatter.format(d);
    }
    
    @Override
    public void incluir(Tarefa tarefa) throws DaoException {
        if(tarefa.eNova()){            
            String sql = "INSERT INTO tb_tarefas (data, descricao, prioridade, " + 
                                                "categoria, situacao) VALUES (";
            
            int cat = tarefa.getCategoria().ordinal();
            int sit = tarefa.getSituacao().ordinal();
            
            sql += "'" + toSqlDate(tarefa.getData()) + "', ";
            sql += "'" + tarefa.getDescricao() + "', ";
            sql += "'" + tarefa.getPrioridade() + "', ";
            sql += "'" + cat + "', ";
            sql += "'" + sit;           
            sql += "');";
            execUpdate(sql);
        }
        
    }

    @Override
    public void atualizar(Tarefa tarefa) throws DaoException {
        String sql = "UPDATE tb_tarefas SET ";
        sql += "data='" + toSqlDate(tarefa.getData()) + "', ";
        sql += "descricao='" + tarefa.getDescricao() + "', ";
        sql += "prioridade='" + tarefa.getPrioridade() + "', ";
        sql += "categoria='" + tarefa.getCategoria().ordinal() + "', ";
        sql += "situacao='" + tarefa.getCategoria().ordinal();
        sql += "' WHERE id = " + tarefa.getId();
        execUpdate(sql);
    }

    @Override
    public void excluir(Tarefa tarefa) throws DaoException {
        String sql = "DELETE from tb_tarefas WHERE id = " + tarefa.getId();
        execUpdate(sql);
    }
    
    private List<Tarefa> getTarefas(String filtro) {
        List<Tarefa> rc = new ArrayList<>();
        String ret = "Dados resgatados com Sucesso";
        String sql = "SELECT * FROM tb_tarefas " + filtro + ";";
        try {
            PreparedStatement stmt = db.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                Integer cat = rs.getInt("categoria");
                Integer sit = rs.getInt("situacao");
                Categoria c = Categoria.from(cat);
                Situacao s = Situacao.from(sit);
                Tarefa t = new Tarefa(rs.getInt("id"), rs.getDate("data"),
                rs.getString("descricao"), rs.getInt("prioridade"), c, s);
                rc.add(t);
            }
        } 
        catch (Exception e) {
            System.out.println(e);
                ret = "ERRO ao tentar ler os dados...";
                System.out.println(
                        ":: ERRO :: Problemas com a leitura de dados no BD...");
        }
        return rc;
    }
    
    private List<Tarefa> getTarefasUpdatingIfNeeded(String filtro) {
        if (null == tarefas || hasChanged || !lastFilter.endsWith(filtro)) {
            tarefas = getTarefas(filtro);
            lastFilter = filtro;
            hasChanged = false;
        }
        return tarefas;
    }

    @Override
    public List<Tarefa> getTarefas() throws DaoException {
        return getTarefasUpdatingIfNeeded("");
    }

    @Override
    public List<Tarefa> getTarefas(Tarefa filtro) throws DaoException {
        String whereClause = "WHERE descricao like '%" + 
                                            filtro.getDescricao() + "%'";
        return getTarefasUpdatingIfNeeded(whereClause);
    }

    private List<Tarefa> getTarefasById(Tarefa filtro) {
        String whereClause = "WHERE id = " + filtro.getId();
        return getTarefasUpdatingIfNeeded(whereClause);
    }

}
