
package br.unip.sicc.tarefas.dao;

import br.unip.sicc.tarefas.model.Tarefa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

public class TarefasJpa  implements TarefasDao{

    private final EntityManager em;
    
    public TarefasJpa(EntityManager em) {
        this.em = em;
    }
   
    @Override
    public void incluir(Tarefa tarefa) throws DaoException {
        try {
            em.getTransaction().begin();
            em.persist(tarefa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DaoException("Erro na inclusão!", ex);
        }
    }

    @Override
    public void atualizar(Tarefa tarefa) throws DaoException {
        try {
            em.getTransaction().begin();
            if(!em.contains(tarefa)){
                em.merge(tarefa);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DaoException("Erro na atualização!", ex);
        }
    }

    @Override
    public void excluir(Tarefa tarefa) throws DaoException {
        try {
            em.getTransaction().begin();
            em.remove(tarefa);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw new DaoException("Deu pau!", ex);
        }
    }

    @Override
    public List<Tarefa> getTarefas() throws DaoException {
        Query namedQuery = em.createNamedQuery("Tarefa.getAll");
        List resultList = namedQuery.getResultList();
        System.out.println(resultList.size());
        return resultList;
    }

    @Override
    public List<Tarefa> getTarefas(Tarefa filtro) throws DaoException {
        Query namedQuery = em.createNamedQuery("Tarefa.getByDescricao");
        namedQuery.setParameter("descricao", "%"+ filtro.getDescricao() + "%");
        return namedQuery.getResultList();
    }

}
