package br.unip.sicc.tarefas.dao;

import java.sql.*;

public class ConnectionFactory {
	
	private Connection con;
	
	public ConnectionFactory(String driver, String url, String login, 
                                                                 String senha) {
            con = null;
            try{                        
                Class.forName(driver);
                //new com.mysql.jdbc.Driver();
                this.con = DriverManager.getConnection(url,login,senha);
            }                
            catch(ClassNotFoundException ex){
                System.out.println(ex);
                System.out.println(
                        ":: ERRO :: Driver JDBC nao encontrado na aplicacao!");
            }                        
            catch(SQLException ex){
                System.out.println(ex);
                System.out.println(
                        ":: ERRO :: Problemas na conexao com a fonte de dados");
            }catch(Exception ex){
                System.out.println(ex);                
                System.out.println(
                                   ":: ERRO :: Outros problemas na conexao...");
            }            
	}
	
	public Connection getConnection() {		
            return this.con;
	}

}