
package br.unip.sicc.tarefas.dao;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;


public class JpaUtil {
    
    private static EntityManager em;
    
    public static EntityManager getEntityManager(){
        if( em == null){
            em = Persistence.createEntityManagerFactory("TarefasPU").createEntityManager();
        }
        return em;
    }

    public static void fechar() {
        em.close();
    }
    
}
