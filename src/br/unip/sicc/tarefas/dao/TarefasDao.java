package br.unip.sicc.tarefas.dao;

import br.unip.sicc.tarefas.model.Tarefa;
import java.util.List;

public interface TarefasDao {
    
    void incluir(Tarefa tarefa) throws DaoException;
    
    void atualizar(Tarefa tarefa) throws DaoException;
    
    void excluir(Tarefa tarefa) throws DaoException;
    
    List<Tarefa> getTarefas() throws DaoException;    
    
    List<Tarefa> getTarefas(Tarefa filtro) throws DaoException;
      
    
}
