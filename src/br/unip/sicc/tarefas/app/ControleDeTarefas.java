package br.unip.sicc.tarefas.app;

import br.unip.sicc.tarefas.model.Tarefa;
import br.unip.sicc.tarefas.view.PainelBuscaTarefas;
import br.unip.sicc.tarefas.view.PainelCadastroTarefas;
import br.unip.sicc.tarefas.view.TelaControleDeTarefas;
import javax.swing.SwingUtilities;

public class ControleDeTarefas {
    private static ControleDeTarefas instance;

    private TelaControleDeTarefas telaControleDeTarefas = null;
    private PainelBuscaTarefas painelBuscaTarefas = null;
    private PainelCadastroTarefas painelCadastroTarefas = null;

    private ControleDeTarefas() {
    }

    public static ControleDeTarefas getInstance() {
        if (instance == null) {
            instance = new ControleDeTarefas();
        }
        return instance;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                getInstance().getTelaControleDeTarefas();
            }

        });
    }

    public TelaControleDeTarefas getTelaControleDeTarefas() {
        if (telaControleDeTarefas == null) {
            telaControleDeTarefas = new TelaControleDeTarefas(this);
        }
        return telaControleDeTarefas;
    }

    public PainelBuscaTarefas getPainelBuscaTarefas() {
        if (painelBuscaTarefas == null) {
            painelBuscaTarefas = new PainelBuscaTarefas(this);
        }

        return painelBuscaTarefas;
    }

    public PainelCadastroTarefas getPainelCadastroTarefas() {
        if (painelCadastroTarefas == null) {
            painelCadastroTarefas = new PainelCadastroTarefas(this);
        }

        return painelCadastroTarefas;
    }

    public void setTarefa(Tarefa tarefaSelecionada) {
        getPainelCadastroTarefas().setTarefa(tarefaSelecionada);
    }

    public void excluirTarefa(Tarefa tarefa) {
        getPainelBuscaTarefas().excluirTarefa(tarefa);
    }

    public void atualizaTarefa(Tarefa tarefa) {
        getPainelBuscaTarefas().atualizaTarefas(tarefa);
    }


}
