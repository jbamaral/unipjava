package br.unip.sicc.tarefas.model;

import br.unip.sicc.tarefas.dao.DaoException;
import br.unip.sicc.tarefas.dao.JpaUtil;
import br.unip.sicc.tarefas.dao.TarefasDao;
import br.unip.sicc.tarefas.dao.TarefasJdbc;
import br.unip.sicc.tarefas.dao.TarefasJdbcMysql;
import br.unip.sicc.tarefas.dao.TarefasJpa;
import br.unip.sicc.tarefas.dao.TarefasList;
import java.util.List;

public class GerenciadorTarefas {
    
    private static GerenciadorTarefas instance;
    //private final TarefasDao dao = new TarefasJdbcMysql();
    private final TarefasDao dao = new TarefasJdbc();
    //private final TarefasDao dao = new TarefasList();
    //private final TarefasDao dao = new TarefasJpa(JpaUtil.getEntityManager());
    private GerenciadorTarefas() {
    }

    public static GerenciadorTarefas getInstance() {
        if(instance==null){
            instance = new GerenciadorTarefas();                    
        }
        return instance;
    }
    
    public void salvar(Tarefa tarefa) throws DaoException{
        if(tarefa.eNova()){
            dao.incluir(tarefa);
        }else{
            dao.atualizar(tarefa);
        }
    }
    
    public void excluir(Tarefa tarefa) throws DaoException{
        dao.excluir(tarefa);
    }
    
    public Tarefa criaNovaTarefa(){
        return new Tarefa();
    }
    
    public List<Tarefa> getTarefas() throws DaoException {
        return dao.getTarefas();
    }
    
    public List<Tarefa> getTarefasFiltradas(Tarefa filtro) throws DaoException {
        return dao.getTarefas(filtro);
    }
    
}
