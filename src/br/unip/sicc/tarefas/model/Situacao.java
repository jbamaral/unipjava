package br.unip.sicc.tarefas.model;

import java.util.HashMap;
import java.util.Map;

public enum Situacao {
    PENDENTE(0), FINALIZADA(1), EM_ANDAMENTO(2), ARQUIVADA(3);   
    
    public final int value;
 
    private Situacao(int value)
    {
        this.value = value;
    }
 
    // Mapping difficulty to difficulty id
    private static final Map<Integer, Situacao> _map = new HashMap<>();
    static
    {
        for (Situacao difficulty : Situacao.values())
            _map.put(difficulty.value, difficulty);
    }
 
    
    public static Situacao from(int value)
    {
        return _map.get(value);
    }    
}
