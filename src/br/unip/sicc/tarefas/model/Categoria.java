package br.unip.sicc.tarefas.model;

import java.util.HashMap;
import java.util.Map;

public enum Categoria {
    TRABALHO(0), ESTUDO(1), PESSOAL(2);
    
  
    public final int value;
 
    private Categoria(int value)
    {
        this.value = value;
    }
 
    // Mapping difficulty to difficulty id
    private static final Map<Integer, Categoria> _map = new HashMap<>();
    static
    {
        for (Categoria difficulty : Categoria.values())
            _map.put(difficulty.value, difficulty);
    }
 
    
    public static Categoria from(int value)
    {
        return _map.get(value);
    }
    
}
