package br.unip.sicc.tarefas.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_TAREFAS")
@NamedQueries(
        {@NamedQuery(name="Tarefa.getAll", query = "Select t from Tarefa t" ),
         @NamedQuery(name="Tarefa.getByDescricao", query = "Select t from Tarefa t where t.descricao like :descricao" 
         )}
)
public class Tarefa implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    private Integer id = 0;
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date data = new Date();
    @Column(nullable = false)
    private String descricao = "";
    @Column(nullable = false)
    private Integer prioridade = 1;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)     
    private Categoria categoria = Categoria.PESSOAL;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)     
    private Situacao situacao = Situacao.PENDENTE;

    private static final Integer[] prioridadesPadrao;
    static{
        prioridadesPadrao = new Integer[]{1,2,3,4,5};
    }

    public Tarefa() {
    }    
    
    public Tarefa(Integer id, Date data, String descricao, Integer prioridade, Categoria categoria, Situacao situacao) {
        this.id = id;
        this.data = data;
        this.descricao = descricao;
        this.prioridade = prioridade;
        this.categoria = categoria;
        this.situacao = situacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(Integer prioridade) {
        this.prioridade = prioridade;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Situacao getSituacao() {
        return situacao;
    }

    public void setSituacao(Situacao situacao) {
        this.situacao = situacao;
    }

    public static Integer[] getPrioridadesPadrao() {
        return prioridadesPadrao;
    }

    @Override
    public String toString() {
        return "Tarefa{" + "id=" + id + ", data=" + data + ", descricao=" + descricao + ", prioridade=" + prioridade + ", categoria=" + categoria + ", situacao=" + situacao + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.data);
        hash = 97 * hash + Objects.hashCode(this.descricao);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tarefa other = (Tarefa) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        return true;
    }
    
    public boolean eNova(){
        if(this.getId().equals(0)){
            return true;
        }else{
            return false;
        }
    }

}
