/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unip.sicc.tarefas.view;
import java.util.Date;
import java.util.Properties;
import javax.swing.JPanel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

    


/**
 *
 * @author johna
 */
public class JDatePickerWrapper {
    private final JDatePickerImpl dp;
    UtilDateModel dm;

    public JDatePickerWrapper() {
        
        JDatePanelImpl dpn;
        DateLabelFormatter dlf;
        dm = new UtilDateModel();
        
        Properties p = new Properties();
        p.put("text.today", "Hoje");
        p.put("text.month", "Mes");
        p.put("text.year", "Ano");

        dpn = new JDatePanelImpl(new PortugueseDateSymbols(),dm, p);
        dpn.revalidate();
        dlf = new DateLabelFormatter();
        dp = new JDatePickerImpl(dpn, dlf );
        dp.setTextEditable(false);        
        dm.setSelected(true);
    }
    
    void addToPannel(JPanel pannel) {
        pannel.add(dp);
    }
    
    void setDate(Date newDate) {
        dm.setValue(newDate);
    }
    Date getDate() {
        return dm.getValue();
    }
            

}
