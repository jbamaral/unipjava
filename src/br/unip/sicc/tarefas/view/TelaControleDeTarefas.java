package br.unip.sicc.tarefas.view;

import br.unip.sicc.tarefas.app.ControleDeTarefas;
import br.unip.sicc.tarefas.model.GerenciadorTarefas;
import br.unip.sicc.tarefas.model.Tarefa;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class TelaControleDeTarefas extends JFrame {
    
    private static final String MSG_SOBRE = 
            "Software desenvolvido para aplicar Orientação \n"
                                + "Objetos com Java";
    private static final long serialVersionUID = 1920920852601222143L;
    
    private final PainelCadastroTarefas painelCadastroTarefas;
    private final PainelBuscaTarefas painelBuscaTarefas;
    private JMenuBar barraDeMenu;
    private JMenu menuOpcoes, menuAjuda;
    private final ControleDeTarefas app;    

    public TelaControleDeTarefas(ControleDeTarefas app) {
        this.app = app;
        this.setTitle("Controle de Tarefas");
        this.setSize(1000, 600);
        this.setLocationRelativeTo(null);
        this.addWindowListener(new WindowActions());

        painelCadastroTarefas = app.getPainelCadastroTarefas();
        painelBuscaTarefas = app.getPainelBuscaTarefas();
        this.setJMenuBar(geraBarraDeMenu());
        this.add(painelBuscaTarefas, BorderLayout.CENTER);
        this.add(painelCadastroTarefas, BorderLayout.EAST);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    
    private JMenuBar geraBarraDeMenu(){
        barraDeMenu = new JMenuBar();
        menuOpcoes = new JMenu("Opções");
        menuOpcoes.setMnemonic(KeyEvent.VK_P);
        menuAjuda = new JMenu("Ajuda");
        menuAjuda.setMnemonic(KeyEvent.VK_H);
        barraDeMenu.add(menuOpcoes);
        barraDeMenu.add(menuAjuda);
        JMenuItem itemNovo = new JMenuItem("Novo");
        itemNovo.setMnemonic(KeyEvent.VK_N);
        itemNovo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Tarefa t = GerenciadorTarefas.getInstance().criaNovaTarefa();
                app.setTarefa(t);
            }
        });
        JMenuItem itemSair = new JMenuItem("Sair");
        itemSair.setMnemonic(KeyEvent.VK_S);
        itemSair.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menuOpcoes.add(itemNovo);
        menuOpcoes.add(itemSair);
        JMenuItem itemSobre = new JMenuItem("Sobre");
        itemSobre.setMnemonic(KeyEvent.VK_B);
        itemSobre.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {                
                JOptionPane.showMessageDialog(null, 
                                MSG_SOBRE,
                                "Sobre",
                                JOptionPane.INFORMATION_MESSAGE
                        );
            }
        });
        menuAjuda.add(itemSobre);
        return barraDeMenu;
    }

    private class WindowActions implements WindowListener {
        @Override
        public void windowOpened(WindowEvent e) {
            System.out.println("windowOpened");
        }

        @Override
        public void windowClosing(WindowEvent e) {
            System.out.println("windowClosing");
        }

        @Override
        public void windowClosed(WindowEvent e) {
            System.out.println("windowClosed");
        }

        @Override
        public void windowIconified(WindowEvent e) {
            System.out.println("windowIconified");
        }

        @Override
        public void windowDeiconified(WindowEvent e) {
            System.out.println("windowDeiconified");
        }

        @Override
        public void windowActivated(WindowEvent e) {
            System.out.println("windowActivated");
        }

        @Override
        public void windowDeactivated(WindowEvent e) {
            System.out.println("windowDeactivated");
        }        
    }
}
