/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unip.sicc.tarefas.view;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JFormattedTextField.AbstractFormatter;

/**
 *
 * @author jbamaral
 */
public class DateLabelFormatter extends AbstractFormatter {
    private static final long serialVersionUID = -4830586803388532218L;
 
    private final String datePattern = "EEEEE, dd 'de' MMMMM 'de' yyyy";
    private final SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

    public DateLabelFormatter() {
        super();
        dateFormatter.setDateFormatSymbols(new PortugueseDateSymbols());
    }
    @Override
    public Object stringToValue(String text) throws ParseException {
        return dateFormatter.parseObject(text);
    }
 
    @Override
    public String valueToString(Object value) throws ParseException {
        if (value != null) {
            Calendar cal = (Calendar) value;
            return dateFormatter.format(cal.getTime());
        }
         
        return "";
    }
 
}

