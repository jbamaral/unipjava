package br.unip.sicc.tarefas.view;

import br.unip.sicc.tarefas.app.ControleDeTarefas;
import br.unip.sicc.tarefas.dao.DaoException;
import br.unip.sicc.tarefas.model.GerenciadorTarefas;
import br.unip.sicc.tarefas.model.Tarefa;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

public class PainelBuscaTarefas extends JPanel {
    private static final long serialVersionUID = -2756145918683977664L;

    private JLabel lblBusca;
    private JTextField txtBusca;
    private JButton butBusca;
    private JTable tabelaTarefas;
    private JScrollPane scroll;
    private final ControleDeTarefas app;

    public PainelBuscaTarefas(ControleDeTarefas app) {
        this.app = app;
        this.setLayout(new BorderLayout());
        this.add(geraPainelBuscas(), BorderLayout.PAGE_START);
        this.add(geraPainelTabela(), BorderLayout.CENTER);
    }

    private JPanel geraPainelBuscas() {
        JPanel painel = new JPanel(
                new FlowLayout(FlowLayout.CENTER, 5, 10));
        lblBusca = new JLabel("Descricão:");
        txtBusca = new TarefaTextField(20);
        txtBusca.addKeyListener(new TxtBuscaListener());
        butBusca = new JButton("Buscar");
        butBusca.setMnemonic(KeyEvent.VK_B);
        butBusca.addActionListener(new BuscaListener());        
        painel.add(lblBusca);
        painel.add(txtBusca);
        painel.add(butBusca);
        return painel;
    }

    private JScrollPane geraPainelTabela() {
        try {
            TarefaTableModel tarefaTableModel;
            tarefaTableModel = new TarefaTableModel(
                                 GerenciadorTarefas.getInstance().getTarefas());
            tabelaTarefas = new JTable(tarefaTableModel);
            tabelaTarefas.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tabelaTarefas.getSelectionModel().addListSelectionListener(
                                                   new ListSelectionListener() {

                @Override
                public void valueChanged(ListSelectionEvent e) {
                    if (e.getValueIsAdjusting()) {
                        return;
                    }
                    int linha = tabelaTarefas.getSelectedRow();
                    if (-1!=linha) {
                        TarefaTableModel  model = 
                                    (TarefaTableModel) tabelaTarefas.getModel();
                        Tarefa tarefaSelecionada = model.getTarefa(linha);
                        app.setTarefa(tarefaSelecionada);
                    }
                    else app.setTarefa(new Tarefa());
                }
            });
            scroll = new JScrollPane(tabelaTarefas);
            return scroll;
        } catch (DaoException ex) {
            JOptionPane.showMessageDialog(this, 
                                        "Não foi possível carregar as tarefas");
            scroll = new JScrollPane(new JTable());
            return scroll;
        }
    }

    public void atualizaTarefas(Tarefa tarefa) {
        TarefaTableModel model = (TarefaTableModel) tabelaTarefas.getModel();
        model.update(tarefa);
        tabelaTarefas.repaint();
        tabelaTarefas.revalidate();
    }

    public void excluirTarefa(Tarefa tarefa) {
        TarefaTableModel model = (TarefaTableModel) tabelaTarefas.getModel();
        int rowToSet = tabelaTarefas.getSelectedRow();
        model.remove(tarefa);        
        tabelaTarefas.revalidate();
        tabelaTarefas.repaint();
        if (tabelaTarefas.getRowCount()>0) {
            ListSelectionModel smodel = tabelaTarefas.getSelectionModel();
            if (rowToSet>0) --rowToSet;            
            else smodel.clearSelection();
            smodel.setSelectionInterval(rowToSet, rowToSet); 
        }  else app.setTarefa(new Tarefa());
    }
    
    void busca() {
        BuscaTarefas busca = new BuscaTarefas();
        Thread t = new Thread(busca);
        t.start();
    }

    private class BuscaListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            busca();
        }
    }
    
    private class TxtBuscaListener implements KeyListener {
        @Override
        public void keyTyped(KeyEvent e) {
        }
        @Override
        public void keyPressed(KeyEvent e) {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                busca();
            }
        }

        @Override
        public void keyReleased(KeyEvent e) {
        }
    }

    private class BuscaTarefas implements Runnable {

        @Override
        public void run() {
            // Processo demorado
            Tarefa filtro = new Tarefa();
            filtro.setDescricao(txtBusca.getText());
            try {
                List<Tarefa> tarefasFiltradas = 
                   GerenciadorTarefas.getInstance().getTarefasFiltradas(filtro);
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        tabelaTarefas.setModel(
                                        new TarefaTableModel(tarefasFiltradas));
                    }
                });

            } catch (DaoException ex) {
                JOptionPane.showMessageDialog(null, 
                                         "Não foi possível filtrar as tarefas");
            }

        }

    }

}
