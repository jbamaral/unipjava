/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unip.sicc.tarefas.view;

import javax.swing.JTextField;
import javax.swing.text.Document;

/**
 *
 * @author Batman
 */
public class TarefaTextField extends JTextField{
    private static final long serialVersionUID = 3105852971237282518L;
    
    
    {
        this.addFocusListener(new DestacaFocoHandler());
    }

    public TarefaTextField() {
        super();
    }

    public TarefaTextField(String text) {
        super(text);
    }

    public TarefaTextField(int columns) {
        super(columns);
    }

    public TarefaTextField(String text, int columns) {
        super(text, columns);
    }

    public TarefaTextField(Document doc, String text, int columns) {
        super(doc, text, columns);
    }
    
}
