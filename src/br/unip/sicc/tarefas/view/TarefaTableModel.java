package br.unip.sicc.tarefas.view;

import br.unip.sicc.tarefas.model.Categoria;
import br.unip.sicc.tarefas.model.Situacao;
import br.unip.sicc.tarefas.model.Tarefa;
import java.util.Date;
import java.util.List;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

public class TarefaTableModel extends AbstractTableModel {
    private static final long serialVersionUID = 6450447191783487402L;

    private final List<Tarefa> tarefas;

    public TarefaTableModel(List<Tarefa> tarefas) {
        this.tarefas = tarefas;
    }

    @Override
    public int getRowCount() {
        return tarefas.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "Id";
            case 1:
                return "Data";
            case 2:
                return "Descrição";
            case 3:
                return "Prioridade";
            case 4:
                return "Categoria";
            case 5:
                return "Situacao";
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return Integer.class;
            case 1:
                return Date.class;
            case 2:
                return String.class;
            case 3:
                return Integer.class;
            case 4:
                return Categoria.class;
            case 5:
                return Situacao.class;
        }
        return Void.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Tarefa tarefa = tarefas.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return tarefa.getId();
            case 1:
                return tarefa.getData();
            case 2:
                return tarefa.getDescricao();
            case 3:
                return tarefa.getPrioridade();
            case 4:
                return tarefa.getCategoria();
            case 5:
                return tarefa.getSituacao();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void addTableModelListener(TableModelListener l) {
    }

    @Override
    public void removeTableModelListener(TableModelListener l) {
    }

    Tarefa getTarefa(int row) {
        return tarefas.get(row);
    }

    public void update(Tarefa tarefa){
        if (!tarefas.contains(tarefa)){
            tarefas.add(tarefa);
        }
    }
    
    public void remove(Tarefa tarefa) {
        if (tarefas.contains(tarefa))  {
            tarefas.remove(tarefa);
        }
    }

}
