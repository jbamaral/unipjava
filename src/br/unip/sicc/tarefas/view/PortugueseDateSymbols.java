/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unip.sicc.tarefas.view;

import java.text.DateFormatSymbols;

/**
 *
 * @author johna
 */
public class PortugueseDateSymbols extends DateFormatSymbols  {
    private static final long serialVersionUID = 3414848598246651576L;

    public PortugueseDateSymbols() {    
        super();
        String[] eras = {"AC","DC",""};
        String[] months={"Janeiro", "Fevereiro", "Março", "Abril", "Maio", 
                     "Junho", "Julho", "Agosto", "Setembro", "Outubro", 
                     "Novembro", "Dezembro",""};
        String[] weekDays={"", "Segunda", "Terça", "Quarta", "Quinta", "Sexta",
                                                      "Sábado", "Domingo" };        
        String[] shortMonths={"Jan", "Fev", "Mar", "Abr", "Mai", 
                     "Jun", "Jul", "Ago", "Set", "Out", 
                     "Nov", "Dez", ""};
        String[] shortWeekDays = 
                          {"", "Dom","Seg", "Ter", "Qua", "Qui", "Sex", "Sáb" };        
        setEras(eras);
        setMonths(months);
        setShortMonths(shortMonths);
        setWeekdays(weekDays);
        setShortWeekdays(shortWeekDays);        
    }
}
