package br.unip.sicc.tarefas.view;

import br.unip.sicc.tarefas.app.ControleDeTarefas;
import br.unip.sicc.tarefas.dao.DaoException;
import br.unip.sicc.tarefas.model.Categoria;
import br.unip.sicc.tarefas.model.GerenciadorTarefas;
import br.unip.sicc.tarefas.model.Situacao;
import br.unip.sicc.tarefas.model.Tarefa;
import com.sun.glass.events.KeyEvent;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerListModel;

public class PainelCadastroTarefas extends JPanel {
    private static final long serialVersionUID = -1633019423206922415L;
    /* declarando os componentes de tela como atributos
     * do painel para ter acesso na classe inteira
     */

    private JLabel lblId, lblData,
            lblDescricao, lblPrioridade,
            lblCategoria, lblSituacao;
    private JTextField txtId, txtDescricao;    
    private JDatePickerWrapper campoData;
    private JSpinner spiPrioridade;
    private JComboBox<Categoria> cboCategoria;
    private JComboBox<Situacao> cboSituacao;
    private JButton butOk, butCancelar, butExcluir;
    private Tarefa tarefa;
    private final ControleDeTarefas app;
    
    /* inicializando os componentes no construtor */
    public PainelCadastroTarefas(ControleDeTarefas app) {
        this.app = app;
        this.setLayout(new BorderLayout());
        this.add(geraPainelCampos(), BorderLayout.NORTH);
        this.add(geraPainelBotoes(), BorderLayout.SOUTH);
    }

    private JPanel geraPainelBotoes() {
        LayoutManager layout
                = new FlowLayout(FlowLayout.RIGHT, 5, 10);
        JPanel painelBotoes
                = new JPanel(layout);
        butOk = new JButton("Ok");
        butOk.setMnemonic(KeyEvent.VK_O);
        butOk.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e){
                Tarefa tarefa = getTarefa();
                try{
                    GerenciadorTarefas.getInstance().salvar(tarefa);
                    setTarefa(tarefa);
                    app.atualizaTarefa(tarefa);                          
                }catch(DaoException ex){
                    JOptionPane.showMessageDialog(null, "Não foi possível salvar");
                }
            }
        });
        
        butCancelar = new JButton("Cancelar");
        butCancelar.setMnemonic(KeyEvent.VK_C);
        butCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                setTarefa(tarefa);
            }
        });
        
        butExcluir = new JButton("Excluir");
        butExcluir.setMnemonic(KeyEvent.VK_X);
        butExcluir.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    GerenciadorTarefas.getInstance().excluir(tarefa);
                    app.excluirTarefa(tarefa);
                }catch(DaoException ex){
                    JOptionPane.showMessageDialog(null, "Não foi possível excluir");
                }
            }
        });
        painelBotoes.add(butOk);
        painelBotoes.add(butCancelar);
        painelBotoes.add(butExcluir);
        return painelBotoes;

    }

    private JPanel geraPainelCampos() {        
        JPanel painel
                = new JPanel(new BorderLayout(10, 10));
        JPanel painelLabels
                = new JPanel(new GridLayout(6, 1, 5, 5));
        JPanel painelCampos
                = new JPanel(new GridLayout(6, 1, 5, 5));
        lblId = new JLabel("Id: ");
        txtId = new TarefaTextField("--0--");
        txtId.setEditable(false);
        lblData = new JLabel("Data: ");
        lblDescricao = new JLabel("Descrição: ");
        txtDescricao = new TarefaTextField();
        txtDescricao.setColumns(30);
        lblPrioridade = new JLabel("Prioridade: ");
        SpinnerListModel prioridadeModel
                = new SpinnerListModel(Tarefa.getPrioridadesPadrao());
        spiPrioridade = new JSpinner(prioridadeModel);
        lblCategoria = new JLabel("Categoria: ");
        cboCategoria = new JComboBox<Categoria>(Categoria.values());
        lblSituacao = new JLabel("Situação: ");
        cboSituacao = new JComboBox<Situacao>(Situacao.values());
        painelLabels.add(lblId);
        painelCampos.add(txtId);
        painelLabels.add(lblData);
        campoData = new JDatePickerWrapper();
        campoData.addToPannel(painelCampos);        
        painelLabels.add(lblDescricao);
        painelCampos.add(txtDescricao);
        painelLabels.add(lblPrioridade);
        painelCampos.add(spiPrioridade);
        painelLabels.add(lblCategoria);
        painelCampos.add(cboCategoria);
        painelLabels.add(lblSituacao);
        painelCampos.add(cboSituacao);        
        painel.add(new JLabel(" "),BorderLayout.NORTH);
        painel.add(new JLabel(" "),BorderLayout.EAST);
        painel.add(painelLabels,BorderLayout.WEST);
        painel.add(painelCampos,BorderLayout.CENTER);
        this.setTarefa(new Tarefa());
        return painel;
    }

    public Tarefa getTarefa() {
        String strId = txtId.getText();
        if("".equals(strId)){
            strId = "0";
        }
        tarefa.setId(Integer.valueOf(strId));
        tarefa.setData(campoData.getDate());
        tarefa.setDescricao(txtDescricao.getText());
        tarefa.setPrioridade((Integer) spiPrioridade.getValue());
        tarefa.setCategoria((Categoria) cboCategoria.getSelectedItem());
        tarefa.setSituacao((Situacao) cboSituacao.getSelectedItem());
        return tarefa;
    }

    public void setTarefa(Tarefa tarefa){
        this.tarefa = tarefa;
        String strId = tarefa.getId().toString();
        txtId.setText(strId);
        txtDescricao.setText(tarefa.getDescricao());
        spiPrioridade.setValue(tarefa.getPrioridade());
        cboCategoria.setSelectedItem(tarefa.getCategoria());
        cboSituacao.setSelectedItem(tarefa.getSituacao());
        campoData.setDate(tarefa.getData());
    }

}
