package br.unip.sicc.tarefas.view;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JComponent;

public class DestacaFocoHandler implements FocusListener{

    @Override
    public void focusGained(FocusEvent e) {
        destaca(e, Color.YELLOW);
    }
    
    @Override
    public void focusLost(FocusEvent e) {
        destaca(e, Color.WHITE);
    }

    private void destaca(FocusEvent e, Color cor){
        if(e.getSource() instanceof JComponent){
            JComponent source = (JComponent) e.getSource();
            source.setBackground(cor);
        }
        
    }
    
}
