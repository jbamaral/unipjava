package alpoo;

import java.sql.*;

/*
...............................................
NESTA CLASSE TEMOS OS M�TODOS QUE FAZEM ACESSO DIRETO
AO BANCO DE DADOS.
...............................................
*/

public class AlunoDAO {

	public String select(Aluno a) {
		String ret = "Dados resgatados com Sucesso";
		// ...abrindo a conex�o
		this.abreCon();
		String sql = "SELECT * FROM aluno  WHERE id=?";
		try {
			// ...abrindo um canal de conexao com a linguagem sql
			PreparedStatement stmt = con.prepareStatement(sql);
			// ...setando os valores
			stmt.setLong(1, a.getId());
			// ...executando o comando sql para retornar os valores lidos
			ResultSet rs = stmt.executeQuery();
			a.setNome("");
			a.setRa("");
			a.setIdade(0);
			while (rs.next()) {
				a.setNome(rs.getString("nome"));
				a.setRa(rs.getString("ra"));
				a.setIdade(rs.getInt("idade"));
			}
			stmt.close();
		} catch (Exception e) {
			ret = "ERRO ao tentar ler os dados...";
			System.out.println(":: ERRO :: Problemas com a leitura de dados no BD...");
		}
		// ...fechando a conex�o
		this.fechaCon();
		return ret;
	}

	public String insert(Aluno a) {
		String ret = "Dados Inseridos com Sucesso";
		// ...abrindo a conex�o
		this.abreCon();
		String sql = "insert into aluno	(id,nome,ra,idade) values (?,?,?,?)";
		try {
			// ...abrindo um canal de conexao com a linguagem sql
			PreparedStatement stmt = con.prepareStatement(sql);
			// ...setando os valores
			stmt.setLong(1, a.getId());
			stmt.setString(2, a.getNome());
			stmt.setString(3, a.getRa());
			stmt.setInt(4, a.getIdade());
			// ...executando o comando sql
			stmt.execute();
			stmt.close();
		} catch (Exception e) {
			ret = "ERRO ao tentar inserir os dados...";
			System.out.println(":: ERRO :: Problemas com a inser��o de dados no BD...");
		}
		// ...fechando a conex�o
		this.fechaCon();
		return ret;
	}
	
	//......PARA LIDAR COM O BANCO DE DADOS..........
	//...............................................

	private Connection con;

	private void abreCon() {
		this.con = new ConnectionFactory().getConnection();
	}

	private void fechaCon() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
