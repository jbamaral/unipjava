package alpoo;

import java.sql.*;

public class ConnectionFactory {
	
	private String driver, url, login, senha;
	private Connection con;
	
	public ConnectionFactory() {
		//driver = "mysql-connector-java-5.1.37.com.mysql.jdbc.Driver";
		driver = "com.mysql.jdbc.Driver";
		url = "jdbc:mysql://localhost:3306/aula_alpoo";
		login = "root";
		senha = "unip";
		con = null;
	}
	
	public Connection getConnection() {
		try{                        
			//Class.forName(driver);
                        new com.mysql.jdbc.Driver();
			this.con = DriverManager.getConnection(url,login,senha);
		}
                /*
                catch(ClassNotFoundException ex){
			System.out.println(":: ERRO :: Driver JDBC n�o encontrado na aplica��o!");
		}
                        */
                catch(SQLException ex){
			System.out.println(":: ERRO :: Problemas na conex�o com a fonte de dados");
		}catch(Exception ex){
			System.out.println(":: ERRO :: Outros problemas na conex�o...");
		}
		return this.con;
	}

}