/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import static java.awt.BorderLayout.*;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Calculadora extends JFrame {
    private final String ZERO = "0.0";
    private final JTextField jt = new JTextField(ZERO);
    private final JButton jbIg = new JButton("=");
    private final JButton[] gridButtons = new JButton[16];
    private final String gridSymbols = "123c456+789-.0*/";
    private final JPanel center = new JPanel();
    private final ButtonListener bl = new ButtonListener();
    private final char INVALID_OPERAND = ' ';
    private double currentLeftParameter=0.0;
    private char currentOperand = INVALID_OPERAND; 
    private boolean clearNext = false;

    public Calculadora() {
        Font buttonFont = new Font("Arial", Font.PLAIN, 20);
        for(int i=0; i < gridButtons.length; ++i) {
            gridButtons[i] = new JButton(""+gridSymbols.charAt(i));
            gridButtons[i].setFont(buttonFont);
            gridButtons[i].addActionListener(bl);
        }
    } 
    private void updateText() {
        String newValue = String.format("%.2f", currentLeftParameter);
        jt.setText(newValue.replaceAll(",", "."));
    }
    private void doCalc() {
        double rightParameter = Double.valueOf(jt.getText());
        switch (currentOperand) {
            case '+': 
                currentLeftParameter += rightParameter;
                break;
            case '-': 
                currentLeftParameter -= rightParameter;
                break;
            case '*': 
                currentLeftParameter *= rightParameter;
                break;
            case '/': 
                currentLeftParameter /= rightParameter;
                break;
        }
        updateText();
    }
    private void doAction(char c) {
        String currentText = jt.getText();
        double currentValue = Double.valueOf(currentText);
        if ((c >= '0' && c <= '9')) {            
            if ((currentText.length() > 2 && 0.0 == currentValue) || 
                                                                    clearNext) {
                currentText = "";                
                clearNext = false;
            }
            currentText += c;
            jt.setText(currentText);
        }
        else if (c=='.') {
            if (!currentText.contains(".")) {
                currentText += c;
                jt.setText(currentText);
            } else jt.setText("0.");
            clearNext = false;
        }
        else if (c == 'c') {            
            jt.setText(ZERO);
            currentOperand = INVALID_OPERAND;
            currentLeftParameter = 0.0;
        }
        else if (c == '=') {
            doCalc();
            currentOperand = INVALID_OPERAND;
            clearNext = true;
        }
        else {
            if (currentOperand != INVALID_OPERAND) {
                doCalc();
            }            
            else {
                currentLeftParameter = currentValue;                
            }
            currentOperand = c;
            clearNext = true;
        }
    }
    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) { 
            Object source = e.getSource();
            if (source.getClass()==JButton.class) {
                JButton jb = (JButton) source;
                doAction(jb.getText().charAt(0));
            }
        }
    };    
    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        calc.run();        
    }
    private void run() { 
        Font fontExtra = new Font("Arial", Font.PLAIN, 30);
        this.setSize(250, 350);
        this.jt.setFont(fontExtra);
        this.add(jt,NORTH);
        this.jbIg.setFont(fontExtra);
        this.jbIg.addActionListener(bl);
        this.add(jbIg,SOUTH);
        this.center.setLayout(new GridLayout(4,4));
        for (JButton b: gridButtons) center.add(b);
        this.add(center, CENTER); 
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                System.exit(0);
            }
        });
        this.setVisible(true);
    }
    
}
